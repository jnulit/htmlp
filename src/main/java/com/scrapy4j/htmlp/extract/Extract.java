package com.scrapy4j.htmlp.extract;

import java.util.List;

/**
 * 实现内容提取的接口
 */
interface Extract {
    public List<String> parse(String content);
}
